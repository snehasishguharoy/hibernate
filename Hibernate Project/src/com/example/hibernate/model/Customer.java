package com.example.hibernate.model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.*;

@Entity
@Table(name="customer")
public class Customer {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="customer_id")
	private int customerId;
	
	@Column(name="customer_name")
	private String customerName;
	@ManyToMany
	@JoinTable(name="CUSTOMER_VEHICLE",joinColumns= {@JoinColumn(name="customer_id")},
	inverseJoinColumns= {@JoinColumn(name="rented_vehicle_id")}
			)
	private Collection<RentedVehicle> rentedVehicles=new ArrayList<>();
	
	public Collection<RentedVehicle> getRentedVehicles() {
		return rentedVehicles;
	}

	public void setRentedVehicles(Collection<RentedVehicle> rentedVehicles) {
		this.rentedVehicles = rentedVehicles;
	}

	

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public Customer() {
		super();
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}


}
