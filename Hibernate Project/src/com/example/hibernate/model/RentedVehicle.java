package com.example.hibernate.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="rented_vehicle")
public class RentedVehicle {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="rented_vehicle_id")
	private int rentedVehicleId;
	
	@Column(name="rented_vehicle_name")
	private String rentedVehicleName;
	
	
	@ManyToMany(mappedBy="rentedVehicles")
	private Collection<Customer> customers=new ArrayList<>();

	public int getRentedVehicleId() {
		return rentedVehicleId;
	}

	

	public Collection<Customer> getCustomers() {
		return customers;
	}



	public void setCustomers(Collection<Customer> customers) {
		this.customers = customers;
	}



	public void setRentedVehicleId(int rentedVehicleId) {
		this.rentedVehicleId = rentedVehicleId;
	}

	public String getRentedVehicleName() {
		return rentedVehicleName;
	}

	public void setRentedVehicleName(String rentedVehicleName) {
		this.rentedVehicleName = rentedVehicleName;
	}

	public RentedVehicle() {
		super();
	}
	
	

}
