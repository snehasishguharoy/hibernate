package com.example.hibernate.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.example.hibernate.dto.Address;

@Entity(name = "user_details")
public class UserDetails implements Serializable {

	private static final long serialVersionUID = 2831570951598481031L;
	@Id
	@GeneratedValue
	private int userId;
	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name="street",column=@Column(name="HOME_STREET_NAME")),
		@AttributeOverride(name="city",column=@Column(name="HOME_CITY_NAME")),
		@AttributeOverride(name="pincode",column=@Column(name="HOME_PINCODE")),
		@AttributeOverride(name="state",column=@Column(name="HOME_STATE_NAME")),
	})
	private Address homeAddress;
	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name="street",column=@Column(name="OFFICE_STREET_NAME")),
		@AttributeOverride(name="city",column=@Column(name="OFFICE_CITY_NAME")),
		@AttributeOverride(name="pincode",column=@Column(name="OFFICE_PINCODE")),
		@AttributeOverride(name="state",column=@Column(name="OFFICE_STATE_NAME")),
	})
	private Address officeAddress;
	
	private String userName;
	@Temporal(TemporalType.DATE)
	private Date joinedDate;

	@Lob
	private String description;

	public Address getHomeAddress() {
		return homeAddress;
	}

	public void setHomeAddress(Address homeAddress) {
		this.homeAddress = homeAddress;
	}

	public Address getOfficeAddress() {
		return officeAddress;
	}

	public void setOfficeAddress(Address officeAddress) {
		this.officeAddress = officeAddress;
	}

	public Date getJoinedDate() {
		return joinedDate;
	}

	public UserDetails() {
		super();
	}

	

	@Override
	public String toString() {
		return "UserDetails [userId=" + userId + ", homeAddress=" + homeAddress + ", officeAddress=" + officeAddress
				+ ", userName=" + userName + ", joinedDate=" + joinedDate + ", description=" + description + "]";
	}

	public void setJoinedDate(Date joinedDate) {
		this.joinedDate = joinedDate;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
