package com.example.hibernate.main;

import org.hibernate.Session;

import com.example.hibernate.config.HibernateCnfg;
import com.example.hibernate.model.Company;
import com.example.hibernate.model.Vehicle;
import com.example.hibernate.model.Worker;

public class CascadeMain {

	public static void main(String[] args) {
		Session session = HibernateCnfg.getSession();
		session.beginTransaction();
		Vehicle vehicle=session.get(Vehicle.class,42);
		System.out.println(vehicle);
		session.getTransaction().commit();
		session.close();
		Worker worker=new Worker();
		worker.setWorkerName("Ramesh");
		Worker worker2=new Worker();
		worker2.setWorkerName("Jaga");
		Company company=new Company();
		company.setCompanyName("CTS");
		company.getWorkers().add(worker);
		company.getWorkers().add(worker2);
		
		Session session2=HibernateCnfg.getSession();
		session2.beginTransaction();
		session2.persist(company);
		session2.getTransaction().commit();
		session2.close();


	}

}
