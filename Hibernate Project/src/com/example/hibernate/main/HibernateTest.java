package com.example.hibernate.main;

import org.hibernate.Session;

import com.example.hibernate.config.HibernateCnfg;
import com.example.hibernate.model.Employee;
import com.example.hibernate.model.UserDetails;

public class HibernateTest {

	public static void main(String[] args) {
		UserDetails details = null;
		UserDetails details2 = null;
		Session session = HibernateCnfg.getSession();
		session.beginTransaction();
		details = session.get(UserDetails.class, 17);
		details2 = session.load(UserDetails.class, 17);
		System.out.println("First Session Check Results ###########");
		System.out.println(details.equals(details2));
	//	System.out.println(details);
		session.getTransaction().commit();
		session.close();

		System.out.println("First Session Ended");
		Employee  employee=null;
		Employee employee2=null;
		Session session2 = HibernateCnfg.getSession();
		session2.beginTransaction();
		employee = session2.get(Employee.class,26);
		session2.close();
		System.out.println(employee.getAddresses().size());
	//	System.out.println(details2);
	

	}
}
