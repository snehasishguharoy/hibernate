package com.example.hibernate.main;

import org.hibernate.Session;

import com.example.hibernate.config.HibernateCnfg;
import com.example.hibernate.model.Customer;
import com.example.hibernate.model.RentedVehicle;

public class ManyToManyMappingMain {

	public static void main(String[] args) {
		RentedVehicle rentedVehicle = new RentedVehicle();
		rentedVehicle.setRentedVehicleName("BMW");
		RentedVehicle rentedVehicle2 = new RentedVehicle();
		rentedVehicle2.setRentedVehicleName("Audi");
		Customer customer = new Customer();
		customer.setCustomerName("Tina");
		customer.getRentedVehicles().add(rentedVehicle);
		customer.getRentedVehicles().add(rentedVehicle2);
		Customer customer2 = new Customer();
		customer2.setCustomerName("Lina");
		customer2.getRentedVehicles().add(rentedVehicle);
		customer2.getRentedVehicles().add(rentedVehicle2);
		
		Session session2 = HibernateCnfg.getSession();
		session2.beginTransaction();
		session2.save(rentedVehicle);
		session2.save(rentedVehicle2);
		session2.save(customer);
		session2.save(customer2);
		session2.getTransaction().commit();
		session2.close();

	}

}
