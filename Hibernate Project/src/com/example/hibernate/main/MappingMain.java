package com.example.hibernate.main;

import org.hibernate.Session;

import com.example.hibernate.config.HibernateCnfg;
import com.example.hibernate.model.Person;
import com.example.hibernate.model.Vehicle;

public class MappingMain {

	public static void main(String[] args) {
		
		Vehicle vehicle=new Vehicle();
		vehicle.setVehicleName("BMW");
		Vehicle vehicle2=new Vehicle();
		vehicle2.setVehicleName("Audi");
	
		Person person=new Person();
		person.setName("Barun");
		vehicle.setPerson(person);
		vehicle2.setPerson(person);
		person.getVehicles().add(vehicle);
		person.getVehicles().add(vehicle2);
		Session session=HibernateCnfg.getSession();
		session.beginTransaction();
		session.save(person);
		session.save(vehicle);
		session.save(vehicle2);
		session.getTransaction().commit();
		session.close();
		
	}

}
